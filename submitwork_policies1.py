#!/usr/bin/env python
import argparse
import requests
import random
import json
import string
import time
import re

script_run = [{'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'lpfvvqe'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '512', 'name': 'igyzbwp'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1, 'memory': 512, 'name': 'vgehjlw'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'badyhjx'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '256', 'name': 'lbpvefk'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1, 'memory': 128, 'name': 'hpbycjy'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'lzejzlr'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '1', 'memory': '512', 'name': 'wghryee'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1, 'memory': 512, 'name': 'fobxggk'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'rgnxnpb'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '512', 'name': 'xdojkso'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 2, 'memory': 128, 'name': 'wlbjdpv'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'wuxigul'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '0.75', 'memory': '256', 'name': 'tiafxoq'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 0.75, 'memory': 512, 'name': 'lwsxbxt'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'rdpxejc'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '1024', 'name': 'fczxmts'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 2, 'memory': 1024, 'name': 'ltfpdua'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'wwqblcb'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '512', 'name': 'ltqhsnn'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1.5, 'memory': 1024, 'name': 'qjhxwpc'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'xepkopt'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '1', 'memory': '256', 'name': 'pqbkjpg'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 2, 'memory': 1024, 'name': 'uyftyeo'}, {'framework': 'spark', 'pay_no': 0, 'memory': '500m', 'name': 'kixeyou'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '0.75', 'memory': '256', 'name': 'rpajyiq'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 2, 'memory': 1024, 'name': 'uiesnuc'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'bylmllp'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '1024', 'name': 'violfld'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1, 'memory': 512, 'name': 'elxxoyp'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'beqjeyb'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '256', 'name': 'ctmevzy'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 2, 'memory': 128, 'name': 'vesdqsz'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'csihqxi'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '512', 'name': 'pnaersc'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 0.75, 'memory': 128, 'name': 'pkfjzew'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'vavixsj'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '1024', 'name': 'zefhvgg'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1, 'memory': 128, 'name': 'pwtddvr'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'brtekbe'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '256', 'name': 'pleklys'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 0.75, 'memory': 1024, 'name': 'geldcnd'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'hqiagyn'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '512', 'name': 'sqpqnfh'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 1, 'memory': 128, 'name': 'wjrdhwe'}, {'framework': 'spark', 'pay_no': 1, 'memory': '400m', 'name': 'plxvuir'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '1024', 'name': 'mkmhfnq'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 0.75, 'memory': 512, 'name': 'dqiydlr'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'xsdzylr'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '1024', 'name': 'cyvledq'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 2, 'memory': 128, 'name': 'fygejwp'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'ntwpdrl'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '0.75', 'memory': '512', 'name': 'lrjqghg'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1.5, 'memory': 512, 'name': 'jarnuwb'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'wdvlcmw'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '256', 'name': 'nynsgmw'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 2, 'memory': 512, 'name': 'wvkazqs'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'lfmmhtx'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '256', 'name': 'njsmefp'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 0.75, 'memory': 128, 'name': 'ovaezht'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'inadmua'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '512', 'name': 'gebqrgn'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 1.5, 'memory': 512, 'name': 'ilwivlc'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'mbpoifz'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '1024', 'name': 'sbtemkk'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 0.75, 'memory': 128, 'name': 'mwoxmzt'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'ekselfk'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '1024', 'name': 'mstvurt'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1, 'memory': 1024, 'name': 'seckran'}, {'framework': 'spark', 'pay_no': 1, 'memory': '500m', 'name': 'imxyqum'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '512', 'name': 'mcpsgcb'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1, 'memory': 1024, 'name': 'stnvjxq'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'ytldkpa'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '0.75', 'memory': '512', 'name': 'kmbcqhe'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1.5, 'memory': 1024, 'name': 'tnbzaxn'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'hdsjxsw'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '0.75', 'memory': '256', 'name': 'soyqile'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1.5, 'memory': 128, 'name': 'qwluylg'}, {'framework': 'spark', 'pay_no': 0, 'memory': '500m', 'name': 'btenobh'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '1024', 'name': 'izbquwq'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 2, 'memory': 1024, 'name': 'logqohv'}, {'framework': 'spark', 'pay_no': 1, 'memory': '400m', 'name': 'msjmhzm'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '512', 'name': 'gpqyvvf'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 1, 'memory': 128, 'name': 'hicwler'}, {'framework': 'spark', 'pay_no': 1, 'memory': '400m', 'name': 'qifxpse'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '2', 'memory': '256', 'name': 'xebuoao'}, {'framework': 'marathon', 'pay_no': 2, 'cpu': 0.75, 'memory': 512, 'name': 'pdquzip'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'dqwmzko'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '1024', 'name': 'aypzukp'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 0.75, 'memory': 128, 'name': 'jifopwv'}, {'framework': 'spark', 'pay_no': 0, 'memory': '400m', 'name': 'dhtmsvj'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '2', 'memory': '512', 'name': 'ezvrjhw'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 0.75, 'memory': 512, 'name': 'cdcreum'}, {'framework': 'spark', 'pay_no': 1, 'memory': '1g', 'name': 'bbjlcwo'}, {'framework': 'chronos', 'pay_no': 0, 'cpu': '0.75', 'memory': '512', 'name': 'durxbyw'}, {'framework': 'marathon', 'pay_no': 1, 'cpu': 0.75, 'memory': 512, 'name': 'inpphgk'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'ojojgob'}, {'framework': 'chronos', 'pay_no': 1, 'cpu': '1', 'memory': '1024', 'name': 'ieimtfb'}, {'framework': 'marathon', 'pay_no': 0, 'cpu': 0.75, 'memory': 512, 'name': 'zhtsbow'}, {'framework': 'spark', 'pay_no': 0, 'memory': '1g', 'name': 'jahyrys'}]
parser = argparse.ArgumentParser(description='Process to submit task')
parser.add_argument('integers', metavar='N', type=int,
                    help='an integer for the total')
parser.add_argument("-f", "--format", type=str, help="Choosing format types")

args = parser.parse_args()
totalIndex = args.integers

random.seed(10)

url_spark = "http://13.212.247.154:7077/v1/submissions/create"
url_chronos = "http://13.212.247.154:4400/v1/scheduler/iso8601"
url_marathon = "http://13.212.247.154:8080/v2/apps"

payload_spark_1 = "{\n  \"appResource\" : \"file:/python/spark_hello_world.py\",\n  \"clientSparkVersion\": \"2.4.7\",\n  \"mainClass\": \"org.apache.spark.deploy.SparkSubmit\",\n  \"sparkProperties\" : {\n    \"spark.app.name\" : \"helloworld\",\n    \"spark.submit.deployMode\" : \"cluster\",\n    \"spark.executor.pyspark.memory\":\"500m\",\n    \"spark.executor.memory\":\"500m\",\n    \"spark.executor.cores\": \"1\",\n    \"spark.cores.max\":\"1\",\n    \"spark.mesos.executor.docker.image\" : \"run-spark\",\n    \"spark.mesos.executor.home\" : \"/usr/local/spark/\",\n    \"spark.master\" : \"mesos://spark-mesos:7077\",\n    \"spark.driver.supervise\": \"false\"\n  },\n  \"environmentVariables\": {\n    \"SPARK_ENV_LOADED\": \"1\"\n  },\n  \"action\" : \"CreateSubmissionRequest\",\n  \"appArgs\": [  ]\n}"

payload_spark_2 = "{\n  \"appResource\" : \"file:/python/test1.py\",\n  \"clientSparkVersion\": \"2.4.7\",\n  \"mainClass\": \"org.apache.spark.deploy.SparkSubmit\",\n  \"sparkProperties\" : {\n    \"spark.app.name\" : \"test1\",\n    \"spark.submit.deployMode\" : \"cluster\",\n    \"spark.executor.pyspark.memory\":\"500m\",\n    \"spark.executor.memory\":\"500m\",\n    \"spark.executor.cores\": \"1\",\n    \"spark.cores.max\":\"1\",\n    \"spark.mesos.executor.docker.image\" : \"run-spark\",\n    \"spark.mesos.executor.home\" : \"/usr/local/spark/\",\n    \"spark.master\" : \"mesos://spark-mesos:7077\",\n    \"spark.driver.supervise\": \"false\"\n  },\n  \"environmentVariables\": {\n    \"SPARK_ENV_LOADED\": \"1\"\n  },\n  \"action\" : \"CreateSubmissionRequest\",\n  \"appArgs\": [  ]\n}"

payload_chronos_1 = "{\n    \"schedule\": \"R1/2021-01-25T15:00:00.000Z/PT24H\",\n    \"name\": \"ga_op\",\n    \"description\": \"optimizezation\",\n    \"container\": {\n        \"type\": \"DOCKER\",\n        \"image\": \"python\"\n    },\n    \"cpus\": \"0.5\",\n    \"mem\": \"512\",\n    \"uris\": [\n        \"https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py\"\n    ],\n    \"command\": \"cd $MESOS_SANDBOX && pip3 install matplotlib numpy psutil numpy && python3 optimizatioproject.py\"\n}"

payload_chronos_2="{\n    \"schedule\": \"R1/2021-01-25T15:00:00.000Z/PT24H\",\n    \"name\": \"tensorflow_textclassification\",\n    \"description\": \"optimizezation\",\n    \"container\": {\n        \"type\": \"DOCKER\",\n        \"image\": \"tensorflow/tensorflow:latest\"\n    },\n    \"cpus\": \"0.5\",\n    \"mem\": \"512\",\n    \"uris\": [\n    ],\n    \"command\": \"cd $MESOS_SANDBOX && curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/textclassification.py && pip3 install tensorflow_datasets tensorflow_hub && python3 textclassification.py\",\n    \"environmentVariables\": []\n}"

payload_marathon_1="{\n  \"id\": \"ga\",\n  \"instances\": 1,\n  \"cpus\": 0.25,\n  \"mem\": 128,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"python:3\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'echo \\\"Hello world\\\" > index.html;curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py; ls -al; pip3 install matplotlib numpy psutil numpy; python3 ./optimizatioproject.py ; curl -X DELETE 13.212.247.154:8080/v2/apps/ga'\",\n  \"fetch\": [\n        {\n          \"uri\": \"https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py\",\n          \"executable\": true,\n          \"destPath\": \"optimizatioproject.sh\"\n        }\n    ],\n  \"backoffSeconds\":30000\n}"

payload_marathon_2="{\n  \"id\": \"tensorflowtextclassification\",\n  \"instances\": 1,\n  \"cpus\": 1,\n  \"mem\": 1024,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"tensorflow/tensorflow:latest\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/textclassification.py;ls -al; pip3 install tensorflow_datasets tensorflow_hub; python3 textclassification.py && curl -X DELETE 13.212.247.154:8080/v2/apps/replace'\",\n  \"backoffSeconds\":30000\n}"

payload_marathon_3="{\n  \"id\": \"textsum\",\n  \"instances\": 1,\n  \"cpus\": 1,\n  \"mem\": 1024,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"newsupapat/tftool:latest\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/galaxy_classification/model.py;curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/galaxy_classification/GalaxyZoo1_DR_table2.csv;pip3 install sklearn;python3 model.py && curl -X DELETE 13.212.247.154:8080/v2/apps/replace'\",\n  \"backoffSeconds\":30000\n}"

frameworks_list = ["spark","chronos","marathon"]

list_p_spark = [payload_spark_1, payload_spark_2]
list_p_chronos = [payload_chronos_1, payload_chronos_2]
list_p_marathon = [payload_marathon_1, payload_marathon_2,payload_marathon_3]
N=7

headers = {
    'Content-Type': 'application/json'
}

def SubmitRequest(url, payload, text):
    try:
        response = requests.request(
            "POST", "http://localhost:9001/tasks", headers=headers, data=payload)
        response.raise_for_status()
        print(f"Finish submit -> {text}.")
        return response.text
    except requests.RequestException as e:
        print("OOPS!! General Error")
        print(str(e))
    except requests.exceptions.RequestException as err:
        print(err)
        raise SystemExit(err)


print(f"This Script will run ::: {totalIndex} ::: Times")
arrlistsend = []
arrlistmem = []
for i in range(totalIndex):
    print("-----------------------------------------------------")
    if args.format == 'str':
        fr = frameworks_list[i % 3]
    else:
        fr = random.choice(frameworks_list)
    list_pl = "list_p_" + fr
    url = "url_" + fr
    # payload=random.choice(eval(list_pl))
    payload=eval(list_pl)[script_run[i]["pay_no"]]
    print(payload)
    # payload=script_run[i]["pay_no"]
    pay_no=eval(list_pl).index(payload)
    dictlog = {
        "framework": fr,
        "pay_no": pay_no,
    }
    print("Random pick framework: ", fr,eval(url),"( task no :",pay_no,")")
    if fr == "marathon":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        payload = payload.replace("replace", uq_id)
        y = json.loads(payload)
        cpu_list = [0.75,1,1.5,2]
        mem_list = [128,512,1024]
        cpu_random = script_run[i]["cpu"]
        mem_random = script_run[i]["memory"]
        # cpu_random = random.choice(cpu_list)
        # mem_random = random.choice(mem_list)
        y["id"]=uq_id
        y["cpus"] = cpu_random
        y["mem"] = mem_random
        dictlog['cpu'] = cpu_random
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps({"framework":fr,"cpu":cpu_random,"mem":mem_random,"cmd":{},"body":y}),fr)
    elif fr == "spark":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        y = json.loads(payload)
        mem_list = ["400m","500m","1g"]
        mem_random=script_run[i]["memory"]
        # mem_random=random.choice(mem_list)
        y['sparkProperties']['spark.executor.memory'] = mem_random
        y['sparkProperties']['spark.executor.pyspark.memory'] = mem_random
        y['sparkProperties']['spark.app.name'] = uq_id
        t = str.maketrans('','',string.ascii_uppercase)
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps({"framework":fr,"cpu":1,"mem":int(mem_random[:-1]),"cmd":{},"body":y}),fr)
    elif fr == "chronos":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        y = json.loads(payload)
        cpu_list = ["0.75","1","1.5","2"]
        mem_list = ["256","512","1024"]
        cpu_random = script_run[i]["cpu"]
        mem_random = script_run[i]["memory"]
        y["cpus"] = cpu_random
        y["mem"] = mem_random
        y["name"] = uq_id
        dictlog['cpu'] = cpu_random
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps({"framework":fr,"cpu":float(cpu_random),"mem":float(mem_random),"cmd":{},"body":y}),fr)
    arrlistsend.append(dictlog)
    print("-----------------------------------------------------")
    if i != totalIndex:
        time.sleep(6)
print(arrlistsend)
# with open('./log2/8.log', 'w') as filehandle:
#         filehandle.write('%s\n' % arrlistsend)