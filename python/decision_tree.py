from sklearn.datasets import load_iris
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

iris = load_iris()
df = pd.DataFrame(columns=iris.feature_names, data=iris.data)
df['target'] = iris.target
names = iris.target_names
df['target name'] = np.where( df['target'] == 0 , names[0],  
                    np.where( df['target'] == 1 , names[1], 
                    np.where( df['target'] == 2 , names[2], np.NAN)))

df.head()
plt.title("Iris")
plt.xlabel('sepal length (cm)')	
plt.ylabel('sepal width (cm)')
plt.axis("equal")

names = iris.target_names
group1 = df[df['target name'] == names[0]]
group2 = df[df['target name'] == names[1]]
group3 = df[df['target name'] == names[2]]

plt.scatter(group1['sepal length (cm)'], group1['sepal width (cm)'], c='r', label=names[0])
plt.scatter(group2['sepal length (cm)'], group2['sepal width (cm)'], c='g', label=names[1])
plt.scatter(group3['sepal length (cm)'], group3['sepal width (cm)'], c='b', label=names[2])


plt.legend(loc='upper right')
# plt.show()

# พล็อตกราฟง่ายๆ อีกวิธีหนึ่ง
sns.FacetGrid(df, hue="target name", height=6) \
   .map(plt.scatter, "sepal length (cm)", "sepal width (cm)") \
   .add_legend()

# plt.show()
from sklearn.tree import DecisionTreeClassifier

#dtc = DecisionTreeClassifier(random_state=0, max_depth=3)
dtc = DecisionTreeClassifier()
dtc.fit(iris.data, iris.target)
from sklearn import tree
print(tree.plot_tree(dtc.fit(iris.data, iris.target)))