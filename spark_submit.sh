docker exec -it spark-mesos bash -c '/usr/local/spark/bin/spark-submit \
--master mesos://spark-mesos:7077 \
--conf spark.mesos.executor.docker.image=run-spark \
--conf spark.mesos.executor.home=/usr/local/spark/ \
--conf spark.master.rest.enabled=true \
--name spark_job_3  \
--deploy-mode cluster \
/python/spark_hello_world.py'

docker exec -it spark-mesos bash -c '/usr/local/spark/bin/spark-submit \
--master mesos://13.212.247.154:7077 \
--conf spark.mesos.executor.docker.image=run-spark \
--conf spark.mesos.executor.home=/usr/local/spark/ \
--conf spark.master.rest.enabled=true \
--name spark_job_3  \
--deploy-mode cluster \
/python/spark_hello_world.py'