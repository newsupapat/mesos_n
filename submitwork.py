#!/usr/bin/env python
import argparse
import requests
import random
import json
import string
import time

parser = argparse.ArgumentParser(description='Process to submit task')
parser.add_argument('integers', metavar='N', type=int,
                    help='an integer for the total')
parser.add_argument("-f", "--format", type=str, help="Choosing format types")

args = parser.parse_args()
totalIndex = args.integers

random.seed(10)

url_spark = "http://13.212.247.154:7077/v1/submissions/create"
url_chronos = "http://13.212.247.154:4400/v1/scheduler/iso8601"
url_marathon = "http://13.212.247.154:8080/v2/apps"

payload_spark_1 = "{\n  \"appResource\" : \"file:/python/spark_hello_world.py\",\n  \"clientSparkVersion\": \"2.4.7\",\n  \"mainClass\": \"org.apache.spark.deploy.SparkSubmit\",\n  \"sparkProperties\" : {\n    \"spark.app.name\" : \"helloworld\",\n    \"spark.submit.deployMode\" : \"cluster\",\n    \"spark.executor.pyspark.memory\":\"500m\",\n    \"spark.executor.memory\":\"500m\",\n    \"spark.executor.cores\": \"1\",\n    \"spark.cores.max\":\"1\",\n    \"spark.mesos.executor.docker.image\" : \"run-spark\",\n    \"spark.mesos.executor.home\" : \"/usr/local/spark/\",\n    \"spark.master\" : \"mesos://spark-mesos:7077\",\n    \"spark.driver.supervise\": \"false\"\n  },\n  \"environmentVariables\": {\n    \"SPARK_ENV_LOADED\": \"1\"\n  },\n  \"action\" : \"CreateSubmissionRequest\",\n  \"appArgs\": [  ]\n}"

payload_spark_2 = "{\n  \"appResource\" : \"file:/python/test1.py\",\n  \"clientSparkVersion\": \"2.4.7\",\n  \"mainClass\": \"org.apache.spark.deploy.SparkSubmit\",\n  \"sparkProperties\" : {\n    \"spark.app.name\" : \"test1\",\n    \"spark.submit.deployMode\" : \"cluster\",\n    \"spark.executor.pyspark.memory\":\"500m\",\n    \"spark.executor.memory\":\"500m\",\n    \"spark.executor.cores\": \"1\",\n    \"spark.cores.max\":\"1\",\n    \"spark.mesos.executor.docker.image\" : \"run-spark\",\n    \"spark.mesos.executor.home\" : \"/usr/local/spark/\",\n    \"spark.master\" : \"mesos://spark-mesos:7077\",\n    \"spark.driver.supervise\": \"false\"\n  },\n  \"environmentVariables\": {\n    \"SPARK_ENV_LOADED\": \"1\"\n  },\n  \"action\" : \"CreateSubmissionRequest\",\n  \"appArgs\": [  ]\n}"

payload_chronos_1 = "{\n    \"schedule\": \"R1/2021-01-25T15:00:00.000Z/PT24H\",\n    \"name\": \"ga_op\",\n    \"description\": \"optimizezation\",\n    \"container\": {\n        \"type\": \"DOCKER\",\n        \"image\": \"python\"\n    },\n    \"cpus\": \"0.5\",\n    \"mem\": \"512\",\n    \"uris\": [\n        \"https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py\"\n    ],\n    \"command\": \"cd $MESOS_SANDBOX && pip3 install matplotlib numpy psutil numpy && python3 optimizatioproject.py\"\n}"

payload_chronos_2="{\n    \"schedule\": \"R1/2021-01-25T15:00:00.000Z/PT24H\",\n    \"name\": \"tensorflow_textclassification\",\n    \"description\": \"optimizezation\",\n    \"container\": {\n        \"type\": \"DOCKER\",\n        \"image\": \"tensorflow/tensorflow:latest\"\n    },\n    \"cpus\": \"0.5\",\n    \"mem\": \"512\",\n    \"uris\": [\n    ],\n    \"command\": \"cd $MESOS_SANDBOX && curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/textclassification.py && pip3 install tensorflow_datasets tensorflow_hub && python3 textclassification.py\",\n    \"environmentVariables\": []\n}"

payload_marathon_1="{\n  \"id\": \"ga\",\n  \"instances\": 1,\n  \"cpus\": 0.25,\n  \"mem\": 128,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"python:3\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'echo \\\"Hello world\\\" > index.html;curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py; ls -al; pip3 install matplotlib numpy psutil numpy; python3 ./optimizatioproject.py ; curl -X DELETE 13.212.247.154:8080/v2/apps/ga'\",\n  \"fetch\": [\n        {\n          \"uri\": \"https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/optimizatioproject.py\",\n          \"executable\": true,\n          \"destPath\": \"optimizatioproject.sh\"\n        }\n    ],\n  \"backoffSeconds\":30000\n}"

payload_marathon_2="{\n  \"id\": \"tensorflowtextclassification\",\n  \"instances\": 1,\n  \"cpus\": 1,\n  \"mem\": 1024,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"tensorflow/tensorflow:latest\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/textclassification.py;ls -al; pip3 install tensorflow_datasets tensorflow_hub; python3 textclassification.py && curl -X DELETE 13.212.247.154:8080/v2/apps/replace'\",\n  \"backoffSeconds\":30000\n}"

payload_marathon_3="{\n  \"id\": \"textsum\",\n  \"instances\": 1,\n  \"cpus\": 1,\n  \"mem\": 1024,\n  \"container\": {\n    \"type\": \"DOCKER\",\n    \"docker\": {\n      \"image\": \"newsupapat/tftool:latest\",\n      \"network\": \"BRIDGE\",\n      \"portMappings\": [\n        { \"containerPort\": 0, \"hostPort\": 0, \"protocol\": \"tcp\" }\n      ]\n    }\n  },\n  \"cmd\": \"/bin/bash -c 'curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/galaxy_classification/model.py;curl -OJ https://gitlab.com/newsupapat/mesos_n/-/raw/main/python/galaxy_classification/GalaxyZoo1_DR_table2.csv;pip3 install sklearn;python3 model.py && curl -X DELETE 13.212.247.154:8080/v2/apps/replace'\",\n  \"backoffSeconds\":30000\n}"

frameworks_list = ["spark","chronos","marathon"]

list_p_spark = [payload_spark_1, payload_spark_2]
list_p_chronos = [payload_chronos_1, payload_chronos_2]
list_p_marathon = [payload_marathon_1, payload_marathon_2,payload_marathon_3]
N=7

headers = {
    'Content-Type': 'application/json'
}

def SubmitRequest(url, payload, text):
    try:
        response = requests.request(
            "POST", url, headers=headers, data=payload)
        response.raise_for_status()
        print(f"Finish submit -> {text}.")
        return response.text
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)


print(f"This Script will run ::: {totalIndex} ::: Times")
arrlistsend = []
arrlistmem = []
for i in range(totalIndex):
    print("-----------------------------------------------------")
    if args.format == 'str':
        fr = frameworks_list[i % 3]
    else:
        fr = random.choice(frameworks_list)
    list_pl = "list_p_" + fr
    url = "url_" + fr
    payload=random.choice(eval(list_pl))
    pay_no=eval(list_pl).index(payload)
    dictlog = {
        "framework": fr,
        "pay_no": pay_no,
    }
    print("Random pick framework: ", fr,eval(url),"( task no :",pay_no,")")
    if fr == "marathon":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        payload = payload.replace("replace", uq_id)
        y = json.loads(payload)
        cpu_list = [0.75,1,1.5,2]
        mem_list = [128,512,1024]
        cpu_random = random.choice(cpu_list)
        mem_random = random.choice(mem_list)
        y["id"]=uq_id
        y["cpus"] = cpu_random
        y["mem"] = mem_random
        dictlog['cpu'] = cpu_random
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps(y),fr)
    elif fr == "spark":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        y = json.loads(payload)
        mem_list = ["400m","500m","1g"]
        mem_random=random.choice(mem_list)
        y['sparkProperties']['spark.executor.memory'] = mem_random
        y['sparkProperties']['spark.executor.pyspark.memory'] = mem_random
        y['sparkProperties']['spark.app.name'] = uq_id
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps(y),fr)
    elif fr == "chronos":
        uq_id = ''.join(random.choices(string.ascii_lowercase, k = N))
        y = json.loads(payload)
        cpu_list = ["0.75","1","2"]
        mem_list = ["256","512","1024"]
        cpu_random = random.choice(cpu_list)
        mem_random = random.choice(mem_list)
        y["cpus"] = cpu_random
        y["mem"] = mem_random
        y["name"] = uq_id
        dictlog['cpu'] = cpu_random
        dictlog['memory'] = mem_random
        dictlog["name"] = uq_id
        SubmitRequest(eval(url),json.dumps(y),fr)
    arrlistsend.append(dictlog)
    print("-----------------------------------------------------")
    if i != totalIndex:
        time.sleep(6)
print(arrlistsend)
# with open('./log2/8.log', 'w') as filehandle:
#         filehandle.write('%s\n' % arrlistsend)