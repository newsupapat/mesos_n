#!/bin/bash

TUTORIAL_HOME=$(pwd)

function buildImages() {
    docker build \
    --build-arg AWS_ACCESS_KEY_ID=AKIAYZZRLXV45Z233EOG --build-arg AWS_SECRET_ACCESS_KEY=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 \
    -t tutorial . && \
    docker build --build-arg AWS_ACCESS_KEY_ID=AKIAYZZRLXV45Z233EOG --build-arg AWS_SECRET_ACCESS_KEY=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 \
    -t run-spark -f spark.Dockerfile . && \
    docker build --build-arg AWS_ACCESS_KEY_ID=AKIAYZZRLXV45Z233EOG --build-arg AWS_SECRET_ACCESS_KEY=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 \
    -t airflow -f airflow.Dockerfile . 
}

function doco() { ( docker-compose -f "$TUTORIAL_HOME/docker-compose.yml" "$@" ); }
# function doco2() { (docker-compose -f "./docker-compose.yml" "$@"); }

function docomesos() {
    doco run --rm -d --service-ports --entrypoint bash mesos-slave -c "mesos-slave" ;
}
function start(){
    doco up -d --force-recreate mesos-master chronos marathon
}
function remove(){
    doco down
    docker rm -f $(docker ps -a -q)
    docker volume rm $(docker volume ls -q)
    docomesos
}
function createAirflowDb() {
    if [ ! $(psql -U postgres -h localhost -lqt | cut -d \| -f 1 | grep -qw airflow) ]; then
        docker exec -it postgresql bash -c 'psql -U postgres -c "create database airflow"'
    fi
}
