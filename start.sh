#!/bin/bash

sudo su
yum update -y
yum install -y docker git nano
usermod -a -G docker ec2-user
service docker start
chkconfig docker on

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

git clone 